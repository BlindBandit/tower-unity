using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot : MonoBehaviour {
    public float moveSpeed = 0f;
	public float groundCollisionRadius = 0.25f;
	public float detectorCollisionRadius = 0.1f;
	public float triggerCollisionRadius = 0.25f;
	public float blockDelayAngry = 1.0f;
	public float blockDelayFlip = 1.0f;

	public Transform groundCheck;
	public Transform wallCheck;
	public Transform playerCheck;
    public Transform bottomCheck;
	public Transform upperCheck;
	public Transform centerCheck;
	public Transform backsideCheck;
	public Transform backsideUpperCheck;
	public List<Transform> triggerZone;

	private bool isGrounded;
	private bool isTouchingWall;
	private bool isBacksideTriggered;
	private bool facingRight = true;

	private float flipDelay = 0f;
	private float blockMovementTime = 0f;
	private float angryMoveSpeed = 9f;
	private float defaultMoveSpeed = 2.8f;

	private float start_Y;
	
	private Rigidbody2D rb2D;


	private void Awake() {
		moveSpeed = 0f;
        rb2D = GetComponent<Rigidbody2D>();
    }

	private void Start() {
		if (PlayerPrefs.HasKey("DelayAngry")) {
		    blockDelayAngry = PlayerPrefs.GetFloat("DelayAngry");
		}

		if (PlayerPrefs.HasKey("DelayFlip")) {
		    blockDelayFlip = PlayerPrefs.GetFloat("DelayFlip");
		}

		start_Y = transform.position.y;
	}

	private void Update() {
		if(!LevelManager.Instance.IsInited || GameManager.Instance.IsPause)
		 	return;

		if(blockMovementTime > 0.0f) {
			rb2D.velocity = new Vector2(0.0f, 0.0f);
			blockMovementTime -= Time.deltaTime;
			if(IsPlayerDetected() && !GameManager.Instance.IsExecution) {
            	GameManager.Instance.RobotHackState();
        	}
			return;
		}

		isGrounded = Physics2D.OverlapCircle((Vector2)groundCheck.position, groundCollisionRadius, LayerMask.GetMask("Ground")) 
			|| Physics2D.OverlapCircle((Vector2)groundCheck.position, groundCollisionRadius, LayerMask.GetMask("Wall"));
		isTouchingWall = Physics2D.OverlapCircle((Vector2)wallCheck.position, detectorCollisionRadius, LayerMask.GetMask("Wall"));
		isBacksideTriggered = Physics2D.OverlapCircle((Vector2)backsideCheck.position, triggerCollisionRadius, LayerMask.GetMask("Player"))
			|| Physics2D.OverlapCircle((Vector2)backsideUpperCheck.position, triggerCollisionRadius, LayerMask.GetMask("Player"));

        if(IsPlayerDetected() && !GameManager.Instance.IsExecution) {
            GameManager.Instance.RobotHackState();
        }

		if(IsPlayerInTriggerZone())
			ActivateAngryMode();

		if(isBacksideTriggered) {
			if(flipDelay <= 0.0f && blockMovementTime <= 0.0f)
				StartCoroutine(ActivateDelayedFlip());
		}

		Walk();

		if( (isTouchingWall || !isGrounded) && flipDelay <= 0f && moveSpeed > 0.0f) {
			StartCoroutine(ActivateDelayedFlip());
		}

		if(flipDelay > 0f)
			flipDelay -= Time.deltaTime;
	}


	public void Init(bool isFacingRight, float speed) {
		moveSpeed = speed;
		if(!isFacingRight)
			Flip();
	}
	

	private void Walk() {
		if(transform.position.y != start_Y) {
			transform.position = new Vector3(transform.position.x, start_Y, transform.position.z);
		}

		float moveHorizontal = facingRight ? 1f : -1f;
		rb2D.velocity = new Vector2(moveHorizontal * moveSpeed, 0.0f);
    }

	private void Flip() {
		facingRight = !facingRight;
		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
		//flipDelay = blockDelayFlip + 1f;
	}

	private bool IsPlayerInTriggerZone() {
		foreach(Transform trigger in triggerZone) {
			if(Physics2D.OverlapCircle((Vector2)trigger.position, triggerCollisionRadius, LayerMask.GetMask("Wall")) )
				return false;

			if(Physics2D.OverlapCircle((Vector2)trigger.position, triggerCollisionRadius, LayerMask.GetMask("Player")) )
				return true;
		}

		return false;
	}

	private bool IsPlayerDetected() {
		bool isDetectedBySide = Physics2D.OverlapCircle((Vector2)playerCheck.position, detectorCollisionRadius, LayerMask.GetMask("Player"));
		bool isDetectedByUpper = Physics2D.OverlapCircle((Vector2)upperCheck.position, detectorCollisionRadius, LayerMask.GetMask("Player"));
		bool isDetectedByCenter = Physics2D.OverlapCircle((Vector2)centerCheck.position, detectorCollisionRadius, LayerMask.GetMask("Player"));

		if(isDetectedBySide)
			Debug.Log("detected by side");
		if(isDetectedByUpper)
			Debug.Log("detected by upper");
		if(isDetectedByCenter)
			Debug.Log("detected by center");

		return (isDetectedBySide || isDetectedByUpper || isDetectedByCenter);
	}

	private void ActivateAngryMode() {
		if(moveSpeed >= angryMoveSpeed)
			return;
        blockMovementTime += blockDelayAngry;
		moveSpeed = angryMoveSpeed;
    }

	private IEnumerator ActivateDelayedFlip() {
		blockMovementTime += blockDelayFlip;
		flipDelay = blockDelayFlip + 0.15f;
		yield return new WaitForSeconds(blockDelayFlip);
		Flip();
	}
}
