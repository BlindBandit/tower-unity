using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DebugLevelInfo : MonoBehaviour {
    public TMP_Text debugInfo;

    public void Init(string info) {
        debugInfo.text = info;
    }
}
