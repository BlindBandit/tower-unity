using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Lift : MonoBehaviour {

    public float moveSpeed;

    public List<Vector3> targets;

    private int targetIndex;
    private float distance;
    private float floorTall;

    private bool isInited;

    private Rigidbody2D rb2D;


	private void Awake() {
		rb2D = GetComponent<Rigidbody2D>();
	}
    
    void FixedUpdate() {
        if(!isInited)
            return;

        distance = (targets[targetIndex].y) - transform.position.y;
		
		if (distance > 0) {
            rb2D.velocity += Vector2.up * moveSpeed;
        }
        else {
            rb2D.velocity = new Vector2(0, 0);
        }
    }

    public void Init(List<int> pattern) {
        isInited = false;
        
        targetIndex = 0;
        targets = new List<Vector3>();
        targets.Add(new Vector3(transform.position.x, transform.position.y, transform.position.z));
        floorTall = LevelManager.Instance.distanceBetweenFloors;

        int count = 0;
        for(int i = 0; i < pattern.Count; i++) {
            count += pattern[i];
            targets.Add(new Vector3(transform.position.x, transform.position.y + (floorTall * count - 0.3f), transform.position.z));
        }

        isInited = true;
    }

    public void SetNextTarget() {
        targetIndex = Mathf.Min(targetIndex + 1, targets.Count - 1);
    }

    private void OnTriggerEnter2D(Collider2D other) {
	    if (other.CompareTag("Player")) {
			SetNextTarget();
	    }
	}
}
