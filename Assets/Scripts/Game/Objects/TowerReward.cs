using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class TowerRewardData {
    public int gold;
    public int chips;
    public int key_a;
    public int key_b;
    public int key_c;

    public TowerRewardData(int gold, int chips, int key_a, int key_b, int key_c) {
        this.gold = gold;
        this.chips = chips;
        this.key_a = key_a;
        this.key_b = key_b;
        this.key_c = key_c;
    }
}

public enum TowerRewardType { Gold, Chips, Key_A, Key_B, Key_C }

public class TowerReward : MonoBehaviour {
    public Sprite gold, chips, key_a, key_b, key_c;
    public SpriteRenderer icon;

    public float destroyRadius = 10f;

    TowerRewardData data;
    

    public void Init(int count) {
        switch(GetRandomRewardType()) {
            case TowerRewardType.Gold:
                data = new TowerRewardData(count, 0, 0, 0, 0);
                icon.sprite = gold;
                break;
            case TowerRewardType.Chips:
                data = new TowerRewardData(0, count, 0, 0, 0);
                icon.sprite = chips;
                break;
            case TowerRewardType.Key_A:
                data = new TowerRewardData(0, 0, count, 0, 0);
                icon.sprite = key_a;
                break;
            case TowerRewardType.Key_B:
                data = new TowerRewardData(0, 0, 0, count, 0);
                icon.sprite = key_b;
                break;
            case TowerRewardType.Key_C:
                data = new TowerRewardData(0, 0, 0, 0, count);
                icon.sprite = key_c;
                break;
        }
    }


    public void Pick() {
        Tower.Instance.PickReward(data);
        DestroyAllRewardsInRadius(destroyRadius);
    }


    private TowerRewardType GetRandomRewardType() {
        TowerRewardType[] values = (TowerRewardType[]) System.Enum.GetValues(typeof(TowerRewardType));
        return values[Random.Range(0, values.Length)];
    }

    private void DestroyAllRewardsInRadius(float radius) {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, radius);
        foreach (Collider2D collider in colliders) {
            if (collider.CompareTag("Reward")) {
                Destroy(collider.gameObject);
            }
        }
    }
}
