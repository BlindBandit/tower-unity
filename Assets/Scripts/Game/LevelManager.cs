﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

	public Transform player;

	public GameObject centerWallPrefab;
	public GameObject blockPrefab;
	public GameObject liftPrefab;
	public GameObject debugTextPrefab;
	public GameObject robotPrefab;
	public GameObject rewardPrefab;

	public float currentY;
	public float distanceBetweenFloors;
	public bool IsInited;

	private float startY;

	private List<GameObject> allLevelObjects = new();

	private Dictionary<int, float> spawnMap = new Dictionary<int, float>()
    {
        { 0, -14.0f },
        { 1, -12.352941176470589f },
        { 2, -10.705882352941178f },
		{ 3, -9.058823529411766f },
        { 4, -7.411764705882355f },
        { 5, -5.764705882352944f },
		{ 6, -4.1176470588235325f },
        { 7, -2.470588235294121f },
        { 8, -0.8235294117647092f },
		{ 9, 0.8235294117647092f },
		{ 10, 2.470588235294121f },
        { 11, 4.1176470588235325f },
        { 12, 5.764705882352944f },
		{ 13, 7.411764705882355f },
        { 14, 9.058823529411766f },
        { 15, 10.705882352941178f },
		{ 16, 12.352941176470589f },
        { 17, 14.0f },
    };

	public static LevelManager Instance { get; private set; }

	private void Awake() {
		Instance = this;
		IsInited = false;

		startY = currentY;
	}


	public void InitLevel(LevelData level) {
		IsInited = false;

		DestroyLevel();

		int rowCounter = 0;
		int levelIndex = 0;

		List<int> rewardRows = new List<int>();
		int rewardItemIndex = -1;

		foreach(var levelRow in level.data) {

			if(rowCounter % 10 == 0) {
				GameObject go = Instantiate(debugTextPrefab, new Vector2(spawnMap[9], currentY), Quaternion.identity, transform);
				allLevelObjects.Add(go);
				DebugLevelInfo dli = go.GetComponent<DebugLevelInfo>();
				levelIndex = rowCounter / 10;
				string debugInfo = level.debugInfo[levelIndex];
				dli.Init(debugInfo);
			}

			for(int i = 0; i < levelRow.Count; i++) {

				Vector2 worldPosition = new Vector2(spawnMap[i], currentY);

				string[] rowData = levelRow[i].Split("-");

				if(rowData[0] == "W") {
					GameObject go = Instantiate(centerWallPrefab, worldPosition, Quaternion.identity, transform);
					allLevelObjects.Add(go);
				}

				else if(rowData[0] == "F") {
					GameObject go = Instantiate(blockPrefab, worldPosition, Quaternion.identity, transform);
					allLevelObjects.Add(go);
				}

				else if(rowData[0] == "E") {
					GameObject go = Instantiate(blockPrefab, worldPosition, Quaternion.identity, transform);
					allLevelObjects.Add(go);
					GameObject robot_go = Instantiate(robotPrefab, new Vector2(worldPosition.x, worldPosition.y + 2f), Quaternion.identity, transform);
					allLevelObjects.Add(robot_go);
					Robot robot = robot_go.GetComponent<Robot>();

					if(rowData.Length > 2) {
						bool isFacingRight = rowData[1] == "R";
						float speed = 0.0f;
						float result;
						if (float.TryParse(rowData[2], System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out result)) {
						    speed = result;
						}
						else {
						    speed = rowData[2] == "M" ? 2.8f : 0.0f;
						}
						robot.Init(isFacingRight, speed);
					}
				}

				else if(rowData[0] == "L") {
					List<int> pattern = new List<int>();

					if(rowData.Length > 1) {
						for(int j = 1; j < rowData.Length; j++)
							pattern.Add(int.Parse(rowData[j]));
					}

					GameObject go = Instantiate(liftPrefab, worldPosition, Quaternion.identity, transform);
					allLevelObjects.Add(go);
					Lift lift = go.GetComponent<Lift>();
					lift.Init(pattern);
				}

				else if(rowData[0] == "T" || rowData[0] == "R") {
					GameObject go = Instantiate(blockPrefab, worldPosition, Quaternion.identity, transform);
					allLevelObjects.Add(go);
					GameObject reward_go = Instantiate(rewardPrefab, new Vector2(worldPosition.x, worldPosition.y + 1.5f), Quaternion.identity, transform);
					allLevelObjects.Add(reward_go);
					TowerReward reward = reward_go.GetComponent<TowerReward>();
					reward.Init(Tower.Instance.GetRewardCountByDifficulty(levelIndex));
				}
			}

			rowCounter++;
			currentY += distanceBetweenFloors;
		}

		IsInited = true;
	}


	private void DestroyLevel() {
		foreach(var levelObj in allLevelObjects)
			Destroy(levelObj);

		allLevelObjects.Clear();

		currentY = startY;
	}


	private List<int> GetRewardRowsIndexes(int count, int from, int to) {
		List<int> rows = new List<int>();
		for(int i = 0; i < count; i++) {
			rows.Add(Random.Range(from + Mathf.FloorToInt((to - from)/count) * i, to - Mathf.FloorToInt((to - from)/count) * (count - 1 - i)));
		}
		return rows;
	}

	private int GetRewardIndexInRow(List<string> row) {
		List<int> floorIndexes = FindAllIndexes(row, "F");
		return floorIndexes.Count > 0 ? floorIndexes[Random.Range(0, floorIndexes.Count)] : -1;
	}


	private List<int> FindAllIndexes(List<string> list, string searchString)
    {
        List<int> indexes = new List<int>();

        for (int i = 0; i < list.Count; i++) {
            if (list[i] == searchString) {
                indexes.Add(i);
            }
        }

        return indexes;
    }
}
