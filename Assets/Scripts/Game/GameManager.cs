﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	
	public TowerGameplayState State;

	public bool IsActive => State == TowerGameplayState.Play;
	public bool IsExecution => State == TowerGameplayState.Execution;
	public bool IsPause => State == TowerGameplayState.Pause;

	public GameObject jessicaPlan;
	public GameObject hackRobot;
	public GameObject executionMask;

	public static GameManager Instance { get; private set; }

	private void Awake() {
		Instance = this;
		State = TowerGameplayState.Play;
	}

	public void GameOver() {
		State = TowerGameplayState.Pause;

		PlayerController player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
		player.StopPlayerMovement();

		jessicaPlan.SetActive(true);
	}

	public void RobotHackState() {
		State = TowerGameplayState.Pause;

		PlayerController player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
		player.StopPlayerMovement();

		hackRobot.SetActive(true);
	}

	public void HackRobot() {
		PlayerController player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
		player.TeleportToNextSafeFloor();

		hackRobot.SetActive(false);

		State = IsExecution ? State : TowerGameplayState.Play;
	}

	public void ExecuteTowerPlan() {
		LevelManager.Instance.InitLevel(Tower.Instance.Level);

		jessicaPlan.SetActive(false);
		executionMask.SetActive(true);

		PlayerController player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
		player.SetStartPosition();
		player.StartExecution(Tower.Instance.ActionsLog);

		CameraMovement camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraMovement>();
		camera.SetCameraAtPlayerPosition();

		State = TowerGameplayState.Execution;
	}

	public void PlayAgain() {
		UnityEngine.SceneManagement.SceneManager.LoadScene(0);
	}
}

public enum TowerGameplayState {Play, Pause, Execution}
