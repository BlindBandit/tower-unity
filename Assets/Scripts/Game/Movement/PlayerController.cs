﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float jumpForce = 60f;
	public float multijumpForce = 50f;
	public float moveSpeed = 18f;

	public float groundCollisionRadius = 0.3f;
	public float multiJumpCollisionRadius = 2f;
	public float wallCollisionRadius = 0.3f;
	public float enemyCollisionRadius = 0.1f;

	public Transform groundCheck;
	public Transform wallCheck;
	public Transform upperCheck;
	public Transform bottomCheck;

	private bool isGrounded;
	private bool isMultiJumpAllowed;
	private bool isTouchingWall;
	private bool isTouchingEnemy;
	private bool facingRight = true;
	private bool executionJump = false;

	private float logTimeCounter = 0f;

	private float flipDelay = 0f;

	private float start_X, start_Y;
	
	private Animator animator;
	private Rigidbody2D rb2D;


	private void Awake() {
		animator = GetComponent<Animator>();
		rb2D = GetComponent<Rigidbody2D>();
	}

	private void Start() {
		start_Y = transform.position.y;
		start_X = transform.position.x;

		if (PlayerPrefs.HasKey("PlayerSpeed")) {
		    moveSpeed = PlayerPrefs.GetFloat("PlayerSpeed");
		}
	}

	private void Update() {
		if(!LevelManager.Instance.IsInited || GameManager.Instance.IsPause)
		 	return;

		if(!GameManager.Instance.IsExecution)
			logTimeCounter += Time.deltaTime;

		isGrounded = Physics2D.OverlapCircle((Vector2)groundCheck.position, groundCollisionRadius, LayerMask.GetMask("Ground")) 
			|| Physics2D.OverlapCircle((Vector2)groundCheck.position, groundCollisionRadius, LayerMask.GetMask("Wall"));
		isMultiJumpAllowed = Physics2D.OverlapCircle((Vector2)groundCheck.position, multiJumpCollisionRadius, LayerMask.GetMask("Ground")) || Physics2D.OverlapCircle((Vector2)groundCheck.position, multiJumpCollisionRadius, LayerMask.GetMask("Wall"));
		isTouchingWall = Physics2D.OverlapCircle((Vector2)wallCheck.position, wallCollisionRadius, LayerMask.GetMask("Wall"));
		isTouchingEnemy = Physics2D.OverlapCircle((Vector2)wallCheck.position, enemyCollisionRadius, LayerMask.GetMask("Enemy"))
			|| Physics2D.OverlapCircle((Vector2)upperCheck.position, enemyCollisionRadius, LayerMask.GetMask("Enemy"))
			|| Physics2D.OverlapCircle((Vector2)bottomCheck.position, enemyCollisionRadius, LayerMask.GetMask("Enemy"));

		animator.SetBool("Grounded", isGrounded);

		Walk();

		if(!GameManager.Instance.IsExecution) {
			if(Input.GetMouseButtonDown(0)) {
				if(isGrounded) {
					Jump(false);
					Tower.Instance.AddLogAction(new TowerGameplayAction(logTimeCounter, TowerActionType.Jump, transform.position.x, transform.position.y, transform.localScale.x));
					logTimeCounter = 0f;
				}
				else if(isMultiJumpAllowed) {
					Jump(true);
					Tower.Instance.AddLogAction(new TowerGameplayAction(logTimeCounter, TowerActionType.Jump, transform.position.x, transform.position.y, transform.localScale.x));
					logTimeCounter = 0f;
				}
			}
		}
		else {
			if(executionJump) {
				Jump(!isGrounded);
				executionJump = false;
			}
		}
		
			
		if( (isTouchingWall && flipDelay <= 0f) || isTouchingEnemy)
			Flip();

		if(flipDelay > 0f)
			flipDelay -= Time.deltaTime;
	}

	private void FixedUpdate() {
		if(!LevelManager.Instance.IsInited || !GameManager.Instance.IsActive)
		 	return;

		animator.SetFloat("Speed", Mathf.Abs(1.0f));
	}


	public void TeleportToNextSafeFloor() {
		transform.position = new Vector3(start_X, GetNextSafeFloorPosition(), transform.position.z);

		if(!GameManager.Instance.IsExecution) {
			Tower.Instance.AddLogAction(new TowerGameplayAction(logTimeCounter, TowerActionType.Hack, transform.position.x, transform.position.y, transform.localScale.x));
			logTimeCounter = 0f;
		}
	}

	public void SetStartPosition() {
		transform.position = new Vector3(start_X, start_Y, transform.position.z);
	}

	public void StartExecution(List<TowerGameplayAction> actionsLog) {
		StartCoroutine(ExecuteActionsLog(actionsLog));
	}

	public void StopPlayerMovement() {
		rb2D.velocity = new Vector2(0f, 0f);
	}


	private IEnumerator ExecuteActionsLog(List<TowerGameplayAction> actionsLog) {
		foreach(var action in actionsLog) {
			yield return new WaitForSeconds(action.timeBeforeAction);

			SetPosition(action.pos_X, action.pos_Y, action.flip);

			switch(action.actionType) {
				case TowerActionType.Jump:
					executionJump = true;
					break;
				case TowerActionType.Hack:
					GameManager.Instance.HackRobot();
					break; 
			}
		}
	}

	private void SetPosition(float x, float y, float flip) {
		transform.position = new Vector3(x, y, transform.position.z);
		transform.localScale = new Vector3(flip, transform.localScale.y, transform.localScale.z);
	}
	

	private void Walk() {
		float moveHorizontal = facingRight ? 1f : -1f;
		rb2D.velocity = new Vector2(moveHorizontal * moveSpeed, rb2D.velocity.y);
    }

    private void Jump(bool isMultiJump) {
		if(isMultiJump)
			RoundPlayerPosition();

        rb2D.velocity = new Vector2(rb2D.velocity.x, 0);
        rb2D.velocity += Vector2.up * jumpForce;
    }

	private void Flip() {
		facingRight = !facingRight;
		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
		flipDelay = 0.2f;
	}

	private void RoundPlayerPosition() {
		transform.position = new Vector3(transform.position.x, GetRoundedPositionY(), transform.position.z);
	}

	private float GetRoundedPositionY() {
		int stage = GetCurrentFloor();
		return start_Y + (stage * LevelManager.Instance.distanceBetweenFloors);
	}

	private float GetNextSafeFloorPosition() {
		int nextSafeFloor = GetNextSafeFloor();
		return start_Y + (nextSafeFloor * LevelManager.Instance.distanceBetweenFloors);
	}

	private int GetNextSafeFloor() {
		int stage = GetCurrentFloor() + 1;
		while(stage % 10 != 0)
			stage++;
		return stage;
	}

	private int GetCurrentFloor() {
		float value = (transform.position.y - start_Y) / LevelManager.Instance.distanceBetweenFloors;
		int floorValue = Mathf.FloorToInt(value);
		int ceilValue = Mathf.CeilToInt(value);
		int stage = (value - floorValue) < 0.85f ? floorValue : ceilValue;
		return stage;
	}

	private void OnTriggerEnter2D(Collider2D other) {
	    if(other.CompareTag("Lift")) {
			if(flipDelay <= 0f)
				Flip();
	    }
		else if(other.CompareTag("Reward")) {
			TowerReward reward = other.gameObject.GetComponent<TowerReward>();
			reward.Pick();
		}
	}
}
