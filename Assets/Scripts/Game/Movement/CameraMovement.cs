﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMovement : MonoBehaviour {

	private float speed;
	public float speedMultiple = .05f;

	private Vector3 targetPosition;
	public Transform target;
	public float Y_cam_offset;

	private float distance;

	public float maxDistanceBeforeLose = 7f;

	private float timer;

	void Start() {
		timer = 0;
	}

	void LateUpdate () {
		distance = (target.position.y + Y_cam_offset) - transform.position.y;
		
		if (GameManager.Instance.IsActive && distance < -maxDistanceBeforeLose) {
			GameManager.Instance.GameOver();
		}
		else if (distance > 1) {
			targetPosition = new Vector3(0, target.position.y + Y_cam_offset, transform.position.z);
			transform.position = Vector3.Lerp(transform.position, targetPosition, distance*Time.deltaTime);
		}
		
		timer += Time.deltaTime;
		speed = speedMultiple;
	}

	public void SetCameraAtPlayerPosition() {
		transform.position = new Vector3(0, target.position.y + Y_cam_offset, transform.position.z);
	}
}
