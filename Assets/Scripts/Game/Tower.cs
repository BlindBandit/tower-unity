using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class Tower : MonoBehaviour {
    public LevelData Level;

    public List<TowerGameplayAction> ActionsLog = new();

    private TowerRewardData RewardsCollected;
    private List<TowerRewardDifficultyLevel> RewardsLevels;

    public static Tower Instance { get; private set; }

    [HideInInspector] public UnityEvent onPickReward = new UnityEvent();

	private void Awake() {
		Instance = this;
        RewardsCollected = new TowerRewardData(0, 0, 0, 0, 0);

        RewardsLevels = new List<TowerRewardDifficultyLevel>();
        RewardsLevels.Add(new TowerRewardDifficultyLevel(0, 0, 1, 5));
        RewardsLevels.Add(new TowerRewardDifficultyLevel(1, 1, 1, 5));
        RewardsLevels.Add(new TowerRewardDifficultyLevel(2, 2, 3, 5));
        RewardsLevels.Add(new TowerRewardDifficultyLevel(3, 2, 5, 5));
        RewardsLevels.Add(new TowerRewardDifficultyLevel(4, 1, 6, 10));
        RewardsLevels.Add(new TowerRewardDifficultyLevel(5, 2, 6, 10));
        RewardsLevels.Add(new TowerRewardDifficultyLevel(6, 3, 6, 10));
        RewardsLevels.Add(new TowerRewardDifficultyLevel(7, 4, 10, 10));
        RewardsLevels.Add(new TowerRewardDifficultyLevel(8, 1, 15, 20));
        RewardsLevels.Add(new TowerRewardDifficultyLevel(9, 6, 10, 15));
        RewardsLevels.Add(new TowerRewardDifficultyLevel(10, 5, 15, 20));
        RewardsLevels.Add(new TowerRewardDifficultyLevel(11, 3, 20, 25));
        RewardsLevels.Add(new TowerRewardDifficultyLevel(12, 4, 20, 25));
        RewardsLevels.Add(new TowerRewardDifficultyLevel(13, 5, 25, 25));
        RewardsLevels.Add(new TowerRewardDifficultyLevel(14, 5, 25, 25));
	}

    public void PickReward(TowerRewardData reward) {
        RewardsCollected.gold += reward.gold;
        RewardsCollected.chips += reward.chips;
        RewardsCollected.key_a += reward.key_a;
        RewardsCollected.key_b += reward.key_b;
        RewardsCollected.key_c += reward.key_c;

        onPickReward.Invoke();
    }

    public TowerRewardData GetCollectedRewards() {
        return RewardsCollected;
    }

    public int GetRewardSpawnsCountByDifficulty(int diff) {
        TowerRewardDifficultyLevel level = RewardsLevels.Find(l => l.difficulty == diff);
        return level.spawnsCount;
    }

    public int GetRewardCountByDifficulty(int diff) {
        TowerRewardDifficultyLevel level = RewardsLevels.Find(l => l.difficulty == diff);
        return Random.Range(level.valueMin, level.valueMax);
    }


    public void SetLevel(LevelData level) {
        Level = new LevelData(level.data, level.debugInfo);
    }

    public void AddLogAction(TowerGameplayAction action) {
        ActionsLog.Add(action);
    }
}


public class TowerRewardDifficultyLevel {
    public int difficulty;
    public int spawnsCount;
    public int valueMin, valueMax;

    public TowerRewardDifficultyLevel(int difficulty, int spawnsCount, int valueMin, int valueMax) {
        this.difficulty = difficulty;
        this.spawnsCount = spawnsCount;
        this.valueMin = valueMin;
        this.valueMax = valueMax;
    }
}


public class TowerGameplayAction {
    public float timeBeforeAction;
    public TowerActionType actionType;
    public float pos_X, pos_Y;
    public float flip;

    public TowerGameplayAction(float time, TowerActionType aType, float x, float y, float flip) {
        this.timeBeforeAction = time;
        this.actionType = aType;
        this.pos_X = x;
        this.pos_Y = y;
        this.flip = flip;
    }
}

public enum TowerActionType {Jump, Hack}