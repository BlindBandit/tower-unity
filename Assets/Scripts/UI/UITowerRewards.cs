using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITowerRewards : MonoBehaviour {
    public List<UITowerRewardItem> items;

    void Start() {
        Tower.Instance.onPickReward.AddListener(Refresh);
    }

    void Refresh() {
        foreach(var item in items)
            item.Refresh();
    }
}
