using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITowerRewardItem : MonoBehaviour {
    public TowerRewardType rType;
    public Text count;

    void Start() {
        count.text = "x0";
    }
    
    public void Refresh() {
        TowerRewardData data = Tower.Instance.GetCollectedRewards();
        switch(rType) {
            case TowerRewardType.Gold:
                count.text = "x" + data.gold.ToString();
                break;
            case TowerRewardType.Chips:
                count.text = "x" + data.chips.ToString();
                break;
            case TowerRewardType.Key_A:
                count.text = "x" + data.key_a.ToString();
                break;
            case TowerRewardType.Key_B:
                count.text = "x" + data.key_b.ToString();
                break;
            case TowerRewardType.Key_C:
                count.text = "x" + data.key_c.ToString();
                break;
        }
    }
}
