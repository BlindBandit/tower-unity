using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugInput : MonoBehaviour {
    public Button btnApply;

    public InputField playerSpeed;
    public InputField rageDelay;
    public InputField flipDelay;

    public PlayerController player;


    void Start() {
        btnApply.onClick.AddListener(OnBtnApplyClick);
    }
    
    void OnBtnApplyClick() {
        if (!string.IsNullOrEmpty(playerSpeed.text)) {
            player.moveSpeed = float.Parse(playerSpeed.text, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture);

            PlayerPrefs.SetFloat("PlayerSpeed", player.moveSpeed);
            PlayerPrefs.Save();
        }
        
        if (!string.IsNullOrEmpty(rageDelay.text)) {
            PlayerPrefs.SetFloat("DelayAngry", float.Parse(rageDelay.text, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture));
            PlayerPrefs.Save();
        }

        if (!string.IsNullOrEmpty(flipDelay.text)) {
            PlayerPrefs.SetFloat("DelayFlip", float.Parse(flipDelay.text, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture));
            PlayerPrefs.Save();
        }
    }
}
