using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITowerJessicaPlan : MonoBehaviour {
    public Button btnExecutePlan;
    public Button btnReplay;

    void Start() {
        btnExecutePlan.onClick.AddListener(OnBtnExecutePlanClick);
        btnReplay.onClick.AddListener(OnBtnReplayPlanClick);
    }

    
    void OnBtnExecutePlanClick() {
        GameManager.Instance.ExecuteTowerPlan();
    }

    void OnBtnReplayPlanClick() {
        GameManager.Instance.PlayAgain();
    }
}
