using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITowerHackRobot : MonoBehaviour {
    public Button btnHack;
    public Button btnReplay;

    void Start() {
        btnHack.onClick.AddListener(OnBtnHackClick);
        btnReplay.onClick.AddListener(OnBtnReplayPlanClick);
    }

    
    void OnBtnHackClick() {
        GameManager.Instance.HackRobot();
    }

    void OnBtnReplayPlanClick() {
        GameManager.Instance.PlayAgain();
    }
}
