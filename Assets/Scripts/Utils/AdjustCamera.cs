﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class AdjustCamera : MonoBehaviour {
    public float desiredSize;
    public CanvasScaler canvas;

    Camera cam;

    void Start() {
        cam = GetComponent<Camera>();
    }

    void OnGUI() {
        if (cam.aspect == 0 || canvas.referenceResolution.y == 0)
            return;

        float referenceAspect = canvas.referenceResolution.x / canvas.referenceResolution.y;
        if (cam.aspect < referenceAspect)
            cam.orthographicSize = desiredSize * referenceAspect / cam.aspect;
        else
            cam.orthographicSize = desiredSize;

        canvas.matchWidthOrHeight = Camera.main.aspect < referenceAspect ? 0 : 1;
    }
}
