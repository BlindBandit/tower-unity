using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System.Linq;
using Random = UnityEngine.Random;


[System.Serializable]
public class LevelData {
    public List<List<string>> data;
    public List<string> debugInfo;

    public LevelData(List<List<string>> data, List<string> debugInfo) {
        this.data = data;
        this.debugInfo = debugInfo;
    }
}


public static class LevelParser {
    private static int maxDifficulty = 14;

    private static Dictionary<int, Dictionary<string, string>> levelsTable;


    public static LevelData GenerateLevel(string levelAsset) {
        ReadLevelDataTable(levelAsset);

        List<List<string>> levelData = new List<List<string>>();
        List<string> debugInfo = new List<string>();

        bool isLiftInPreviousSection = false;

        for(int i = 0; i <= maxDifficulty; i++) {
            string randomLevel = "";
            string rawLevelData = "";
            bool isLiftInCurrentSection;
            int counter = 0;
            do {
                randomLevel = GetRandomDictionaryKey(levelsTable[i]);
                rawLevelData = levelsTable[i][randomLevel];
                isLiftInCurrentSection = IsLiftInLevel(rawLevelData);

                counter++;
                if(counter >= levelsTable[i].Count)
                    break;
                    
            } while(isLiftInPreviousSection && isLiftInCurrentSection);

            debugInfo.Add(i.ToString() + "-" + randomLevel);

            string[] levelRows = rawLevelData.Split("&");
            Array.Reverse(levelRows);
            foreach(var levelRow in levelRows) {
                List<string> row = new List<string>();
                string[] rowItems = levelRow.Split(",");
                if(rowItems.Length == 18) {
                    foreach(var item in rowItems)
                        row.Add(item.Trim());
                    levelData.Add(row);
                }
            }

            isLiftInPreviousSection = isLiftInCurrentSection;
        }

        ClearLevelDataTable();

        return new LevelData(levelData, debugInfo);
    }


    private static void ReadLevelDataTable(string levelAsset) {
        ClearLevelDataTable();

        var text = ReplaceMarkers(levelAsset);
        text = Regex.Replace(text, @"""""", "[quote]", RegexOptions.None);
        var matches = Regex.Matches(text, "\"[\\s\\S]+?\"");

        foreach (Match match in matches) {
            text = text.Replace(match.Value, match.Value.Replace("\"", null).Replace(",", "[comma]").Replace("\n", "[newline]"));
        }

        var lines = text.Split(new[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);

        for (var i = 0; i < lines.Length; i++) {
            var columns = lines[i].Split(',').Select(j => j.Trim()).Select(j => j.Replace("[comma]", ",").Replace("[newline]", "\n").Replace("[quote]", "\"")).ToList();
            var key = columns[0];

            for(int j = 1; j < columns.Count; j++) {
                if(columns[j].Length > 10) {
                    levelsTable[j-1].Add(key, columns[j]);
                }
            }
        }
    }

    private static string ReplaceMarkers(string text) {
        return text.Replace("[Newline]", "\n");
    }

    private static void ClearLevelDataTable() {
        levelsTable = new Dictionary<int, Dictionary<string, string>>();
        for(int i = 0; i <= maxDifficulty; i++)
            levelsTable.Add(i, new Dictionary<string, string>());
    }

    private static string GetRandomDictionaryKey(Dictionary<string, string> dictionary) {
        int randomIndex = Random.Range(0, dictionary.Count);
        string randomKey = dictionary.Keys.ElementAt(randomIndex);
        return randomKey;
    }

    private static bool IsLiftInLevel(string level) {
        var matches = Regex.Matches(level, "L-\\d-\\d");
        return matches.Count > 0;
    }
}
