using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System;


public class Net : MonoBehaviour
{
    public static Net Instance { get; private set; }

    public TextAsset levelAsset;

    public string dataUrl;
    //https://drive.google.com/uc?export=download&id=1aid0nQ7USZ-Ya5n5irOvv2dMNEYfAuPO 0.0.2
    //https://drive.google.com/uc?export=download&id=1m2AR63K4ic2QqT_dMcnXAGOzSTXpIXAa 0.0.6
    //https://docs.google.com/spreadsheets/d/1r5v2gI5LxMYSANArvpLA5_nJabW6nmiw856uAkCb-YU/export?format=csv&gid=0
    
    void Start() {
        Instance = this;
        StartCoroutine(LoadData(dataUrl));
        //LoadFromAsset(levelAsset);
    }

    public IEnumerator LoadData(string url) {
        using (WebClient client = new WebClient()) {
            client.DownloadStringCompleted += DownloadStringCompletedEventHandler;
            client.DownloadStringAsync(new Uri(url));
            do yield return new WaitForSeconds(0.5f);
            while (client.IsBusy);
        }
    }

    public void DownloadStringCompletedEventHandler(object sender, DownloadStringCompletedEventArgs e) {
        if (e.Error == null) {
            Debug.Log(e.Result);
            LevelData level = LevelParser.GenerateLevel(e.Result);
            Tower.Instance.SetLevel(level);
            LevelManager.Instance.InitLevel(level);
        }
    }

    public void LoadFromAsset(TextAsset levelAsset) {
        LevelData level = LevelParser.GenerateLevel(levelAsset.text);
        LevelManager.Instance.InitLevel(level);
    }
}


